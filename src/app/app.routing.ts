import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {LogoutComponent} from '@universis/common';
import {ErrorBaseComponent, HttpErrorComponent} from '@universis/common';
import {AuthGuard} from '@universis/common';
import {LangComponent} from './teachers-shared/lang-switch.component';
import {ProfileModule} from './profile/profile.module';

export const routes: Routes = [
  {
    path: 'lang/:id/:route',
    component: LangComponent
  },
  {
    path: 'lang/:id/:route/:subroute',
    component: LangComponent,
    pathMatch: 'prefix'
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule'
      },
      {
        path: 'theses',
        loadChildren: './theses/theses.module#ThesesModule'
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule'
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
