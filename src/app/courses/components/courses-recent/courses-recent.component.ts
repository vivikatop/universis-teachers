import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {ProfileService} from '../../../profile/services/profile.service';
import {ErrorService} from '@universis/common';
import {LoadingService} from '@universis/common';




@Component({
  selector: 'app-courses-recent',
  templateUrl: './courses-recent.component.html',
  styleUrls: ['./courses-recent.component.scss']
})
export class CoursesRecentComponent implements OnInit {
  public recentCourses: any;
  public courseCurrentExams: any;
  public instructor: any;
  viewMode = 'tab1';
  public CoursesSameDep: any;
  public CoursesDiffDep: any;
  public loading = true;



  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private profile: ProfileService,
              private  coursesService: CoursesService,
              private loadingService: LoadingService,
              private errorService: ErrorService) { }

  ngOnInit() {
// show loading
    this.loadingService.showLoading();
    Promise.all([
      this.profile.getInstructor(),
      this.coursesService.getRecentCourses(),
      this.coursesService.getCourseExams(),
      this.coursesService.getCourseCurrentExams()
    ]).then( results => {
      // get instructor results[0]
      this.instructor = results[0];
      // get recent courses results[1]
      this.recentCourses = results[1];
      this.CoursesSameDep = this.recentCourses.filter(x => {
        return x.course.department.name === this.instructor.department.name ;
      });
      this.CoursesDiffDep = this.recentCourses.filter(x => {
        return x.course.department.name !== this.instructor.department.name ;
      });
      // get course exams results[2]
      this.courseCurrentExams = results[2].value;
      // add related exams to each course class
      this.recentCourses.forEach((x) => {
        return x.exams = (this.courseCurrentExams || []).filter((y) => {
          return y.classes.findIndex((z) => {
            return z.courseClass === x.id;
          }) >= 0;
        });
      });
      // hide loading
      this.loadingService.hideLoading();
      // set loading flag
      this.loading = false;
    }).catch( err => {
      // hide loading
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });
  }
}
function checkScreenSize() {
  return window.innerWidth < 500; // The screen size you would like to enable the click;
}
