import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import {StudentsHomeComponent} from './components/students-home/students-home.component';

const routes: Routes = [
  {
    path: '',
    component: StudentsHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
