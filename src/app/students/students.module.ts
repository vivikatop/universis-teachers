import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import { StudentsRoutingModule } from './students-routing.module';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    StudentsRoutingModule,
    TranslateModule
  ],
  declarations: [StudentsHomeComponent]
})
export class StudentsModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/students.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
