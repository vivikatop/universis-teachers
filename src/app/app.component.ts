import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {ConfigurationService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';

@Component({
    selector: 'app-root',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'app';
    constructor(private _router: Router,
                public context: AngularDataContext,
                private _config: ConfigurationService,
                public translate: TranslateService) {
        if (this._config.settings.i18n && this._config.settings.i18n.defaultLocale) {
            translate.setDefaultLang(this._config.settings.i18n.defaultLocale);
        } else {
            translate.setDefaultLang('en');
        }
        translate.use(this._config.currentLocale);
    }
}
